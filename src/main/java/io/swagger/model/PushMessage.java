package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Push;




/**
 * PushMessage
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-30T15:48:47.266Z")

public class PushMessage   {
  private String idAplicativo = null;

  private Push push = null;

  public PushMessage idAplicativo(String idAplicativo) {
    this.idAplicativo = idAplicativo;
    return this;
  }

   /**
   * Application client ID
   * @return idAplicativo
  **/
  @ApiModelProperty(required = true, value = "Application client ID")
  public String getIdAplicativo() {
    return idAplicativo;
  }

  public void setIdAplicativo(String idAplicativo) {
    this.idAplicativo = idAplicativo;
  }

  public PushMessage push(Push push) {
    this.push = push;
    return this;
  }

   /**
   * Get push
   * @return push
  **/
  @ApiModelProperty(required = true, value = "")
  public Push getPush() {
    return push;
  }

  public void setPush(Push push) {
    this.push = push;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PushMessage pushMessage = (PushMessage) o;
    return Objects.equals(this.idAplicativo, pushMessage.idAplicativo) &&
        Objects.equals(this.push, pushMessage.push);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idAplicativo, push);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PushMessage {\n");
    
    sb.append("    idAplicativo: ").append(toIndentedString(idAplicativo)).append("\n");
    sb.append("    push: ").append(toIndentedString(push)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

