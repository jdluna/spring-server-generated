package io.swagger.api;

import io.swagger.model.GeneralResponse;
import org.joda.time.LocalDate;
import java.io.File;
import io.swagger.model.ErrorResponse;
import io.swagger.model.Address;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-30T15:48:47.266Z")

@Api(value = "email", description = "the email API")
public interface EmailApi {

    @ApiOperation(value = "", notes = "Send an email to the destination", response = GeneralResponse.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = GeneralResponse.class),
        @ApiResponse(code = 200, message = "Error", response = GeneralResponse.class) })
    @RequestMapping(value = "/email",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    ResponseEntity<GeneralResponse> send(


@ApiParam(value = "Application client ID", required=true ) @RequestPart(value="idAplicativo", required=true)  String idAplicativo
,


@ApiParam(value = "date time for which message was received", required=true ) @RequestPart(value="sendDate", required=true)  LocalDate sendDate
,


@ApiParam(value = "destination email address", required=true ) @RequestPart(value="destination", required=true)  String destination
,


@ApiParam(value = "subject of the email message", required=true ) @RequestPart(value="subject", required=true)  String subject
,


@ApiParam(value = "cc email address" ) @RequestPart(value="copyDestination", required=false)  String copyDestination
,


@ApiParam(value = "file detail") @RequestPart("file") MultipartFile attachment
);


    @ApiOperation(value = "", notes = "Validate an Email destination", response = GeneralResponse.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = GeneralResponse.class),
        @ApiResponse(code = 200, message = "Error", response = GeneralResponse.class) })
    @RequestMapping(value = "/email/validate",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<GeneralResponse> validate(

@ApiParam(value = "Email address to validate" ,required=true ) @RequestBody Address address

);

}
