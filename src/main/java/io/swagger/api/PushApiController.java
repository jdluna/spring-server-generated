package io.swagger.api;

import io.swagger.model.GeneralResponse;
import io.swagger.model.PushMessage;
import io.swagger.model.ErrorResponse;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-30T15:48:47.266Z")

@Controller
public class PushApiController implements PushApi {

    public ResponseEntity<GeneralResponse> push(

@ApiParam(value = "Push Notifications details" ,required=true ) @RequestBody PushMessage pushMessage

) {
        // do some magic!
        return new ResponseEntity<GeneralResponse>(HttpStatus.OK);
    }

}
