package io.swagger.api;

import io.swagger.model.GeneralResponse;
import io.swagger.model.PushMessage;
import io.swagger.model.ErrorResponse;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-09-30T15:48:47.266Z")

@Api(value = "push", description = "the push API")
public interface PushApi {

    @ApiOperation(value = "", notes = "Send push notitifications", response = GeneralResponse.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = GeneralResponse.class),
        @ApiResponse(code = 200, message = "Error", response = GeneralResponse.class) })
    @RequestMapping(value = "/push",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<GeneralResponse> push(

@ApiParam(value = "Push Notifications details" ,required=true ) @RequestBody PushMessage pushMessage

);

}
